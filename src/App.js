import './App.css';
import Contact from './Components/Contact';
import Home from './Components/Home';
import About from './Components/About';
import Services from './Components/Services';
import Navbar from './Navbar';
import {BrowserRouter as Router , Switch, Route} from 'react-router-dom';
import { Helmet } from 'react-helmet';

function App() {
  return (
    <div className="App">
     <Helmet>
      <title>React-Helmet Demo</title>
      <meta name="description" content="Resct-Helmet-Description" />
      </Helmet>
     <Router>
       <Navbar />
      <div>
      
      <Switch>
      
       
        <Route path = '/' component = {Home} exact/>
        <Route path = '/contact' component = {Contact} exact />
        <Route path = '/about' component = {About} exact/> 
        <Route path = '/services' component = {Services} exact/>

      </Switch>
      </div>
      </Router>
      
    </div>
  );
}


export default App;
