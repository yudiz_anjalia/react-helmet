import  React from "react";
import Helmet from "react-helmet";

function About() {
  return (
  
    <><Helmet>
    <title>About Page</title>
    <meta name="description" content="About page -Description" />
  </Helmet>
  <div className="about">
  <h1>This helmet instance contains the following properties:</h1>
  <h3>
    <li>
      base
    </li>
    <li>
    bodyAttributes
    </li>
    <li>
    htmlAttributes
    </li>
    <li>
    link
    </li>
    <li>
    meta
    </li>
    <li>
    noscript
    </li>
    <li>
    script
    </li>
    <li>
    style
    </li>
    <li>
    title
    </li>
  </h3>
 
    <h2>
    Each property contains toComponent() and toString() methods. Use whichever is appropriate for your environment. <br/>For attributes, use the JSX spread operator on the object returned by toComponent()
    </h2>
    </div>
  </>

);
}
export default About
