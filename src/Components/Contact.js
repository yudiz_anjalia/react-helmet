import  React from "react";
import Helmet from "react-helmet";

function Contact() {
  return (
  
    <><Helmet>
    <title>Contact Page</title>
    <meta name="description" content="About page -Description" />
  </Helmet>
  <div className="contact">
  <h1>Contact Us</h1>
  <h2>npm command-line</h2>
  <h3>If you're having trouble using the npm command-line interface, or you need help with a project that you’re working on,<br/> we recommend that you check out the Software Development board of the GitHub Community forums.
</h3>
<h2>Giving Feedback</h2>
<h3>If you have suggestions for how we can improve npm please open a discussion in our feedback forum.</h3>
<h2>Press Relations</h2>  
<h3>Press inquiries should be addressed to press@npmjs.com</h3>
</div>
</>

);
}
export default Contact