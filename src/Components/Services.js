import  React from "react";
import Helmet from "react-helmet";

function Services() {
  return (
  
    <><Helmet>
    <title>Services Page</title>
    <meta name="description" content="About page -Description" />
  </Helmet>
  <div className="services">
  <h1>Policies</h1>
  <h2>These are the legal policies of npm, Inc.</h2>
  <h3>
     <li>
      Terms of Use
    </li>
    <li>
    Open Source Terms
    </li>
    <li>
    Code of Conduct
    </li>
    <li>
    Private Terms
    </li>
    <li>
    Package Name Disputes
    </li>
    <li>
    npm License
    </li>
    <li>
    Privacy Policy
    </li>
    <li>
    Unpublish Policy
    </li>
  </h3>
  </div>
  </>

);
}
export default Services