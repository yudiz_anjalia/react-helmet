import React from 'react'
import './App.css'


import {Link} from 'react-router-dom'

function Navbar() {

    return <div className='Nav'>
        <>
        
        </>
        
        <ul>
            <Link to = '/services'>
            <li>
                Services
            </li>
            </Link>
            <Link to ='/contact'>
            <li>
                Contact Us
            </li>
            </Link>
            <Link to = '/about'>
                <li>
                    About
                </li>
            </Link>
            <Link to ='/' >
            <li>
                Home
            </li>
            </Link>
        </ul>


    </div>
}

export default Navbar